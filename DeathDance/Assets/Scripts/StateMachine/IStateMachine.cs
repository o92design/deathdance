﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DD_StateMachine
{
  public abstract class IStateMachine : MonoBehaviour
  {
    public string m_name = "State Machine";
    protected IState m_currentState;

    public IState GetCurrentState () { return m_currentState; }
    public bool SetCurrentState (IState p_state) 
    {
      if(m_currentState == null) 
      {
        m_currentState = p_state;
        return true;
      }

      bool enteredState = false;
      if(m_currentState.IsAllowedToEnterState (p_state))
      {
        m_currentState.Exit ();

        m_currentState = p_state;

        m_currentState.Enter ();
        enteredState = true;
      }
      else
      {
        Debug.Log ("Wasn't allowed to enter " + p_state.GetName () + " from " + m_currentState.GetName ());
      }

      return enteredState;
    }
    
    // True if we switched to the state
    // False if we couldn't. For example we're in that state already
    public abstract bool SwitchState (IState p_toState);
    public void UpdateState () { m_currentState.Update (); }
  }
}
