﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DD_StateMachine
{
  public interface IState
  {
    string GetName ();
    void SetName (string p_name);

    bool Initialize ();

    void Enter ();
    void Exit ();

    int GetState ();

    bool AddToAllowedEntryStates (IState p_state);
    bool IsAllowedToEnterState (IState p_toState);

    void Update ();
  }
}


