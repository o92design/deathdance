﻿using UnityEngine;
using UnityEngine.TestTools;
using NUnit.Framework;
using System.Collections;

namespace DD_GameWorld
{
  public class BorderTest
  {
    GameObject m_borderObject;
    GameObject m_entityElder;

    Border m_border;
  
    [SetUp]
    public void SetUp ()
    {
      Debug.Log ("=========================");
      Debug.Log ("TEST - BorderTest");
      Debug.Log ("=========================");

      m_entityElder = new GameObject ("Elder");    
      m_entityElder.AddComponent<DD_Entity.EntityData> ();
      m_entityElder.AddComponent<DD_Entity.PlayerMovement> ();

      m_entityElder.GetComponent<DD_Entity.PlayerMovement> ().Initialize ();

      m_borderObject = new GameObject ();
      m_border = m_borderObject.AddComponent<Border> ();

      m_border.Initialize ();
      Assert.IsNotNull (m_border.m_allowedEntitiesTag);
    }

    [Test]
    public void CheckIfGameObjectIsAllowedThrough ()
    {
      GameObject playerObject = new GameObject ("Player");
      GameObject elderObject  = new GameObject ("Elder");

      playerObject.tag = "Player";
    
      elderObject.tag  = "Elder";

      m_border.m_allowedEntitiesTag.Add (playerObject.tag);
      Assert.IsNotEmpty (m_border.m_allowedEntitiesTag);

      Assert.IsTrue (m_border.IsObjectAllowed (playerObject),
                     "{0} = {1} | with tag {2} was allowed through even though it isn't in the allowed list",
                     playerObject.name,
                     playerObject.GetHashCode (),
                     playerObject.tag);

      Assert.IsFalse (m_border.IsObjectAllowed (elderObject), 
                      "{0} = {1} | with tag {2} was allowed through even though it isn't in the allowed list",
                      elderObject.name,
                      elderObject.GetHashCode (),
                      elderObject.tag);
    }

    [Test]
    public void BlockEntity ()
    {
      Vector3 oldEntityPosition = m_entityElder.transform.position;
      m_border.BlockEntity (m_entityElder);
      Assert.AreNotEqual (oldEntityPosition, m_entityElder.transform.position,
                          "{0} = {1} | hasn't been pushed back by the border",
                          m_entityElder.name,
                          m_entityElder.GetHashCode ());
    }
  }
}
