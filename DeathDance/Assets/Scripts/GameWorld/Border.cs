﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace DD_GameWorld
{
  public class Border : MonoBehaviour
  {
    public List<string> m_allowedEntitiesTag;

    Vector3 m_borderPosition;

    public Material m_material;

    public void Awake ()
    {
      Initialize ();
    }

    public void Initialize ()
    {
      m_borderPosition = transform.position;

      if (m_allowedEntitiesTag == null)
      {
        m_allowedEntitiesTag = new List<string> ();
      }

      // Workaround for unit testing TODO: (kioskars) Fix proper toggling
      if(GetComponent<Renderer> ())
        m_material = GetComponent<Renderer> ().material;
    }

    public void SetColor (Color p_color)
    {
      m_material.color = p_color;
    }

    private void OnTriggerStay (Collider p_collider)
    {
      var entity = p_collider.gameObject;

      if (!IsObjectAllowed (p_collider.gameObject))
        BlockEntity (entity);
    }

    [ExecuteInEditMode]
    public void BlockEntity (GameObject p_entity)
    {
      Vector3 entityPositionOnEnter = p_entity.transform.position;

      entityPositionOnEnter.z += (m_borderPosition.z < entityPositionOnEnter.z) ? .5f : -.5f;
      entityPositionOnEnter.x += (m_borderPosition.x < entityPositionOnEnter.x) ? .5f : -.5f;

      p_entity.transform.SetPositionAndRotation (entityPositionOnEnter, p_entity.transform.rotation);

      try {
        p_entity.GetComponent<DD_Entity.BasicMovement> ().BlockedByBorder (true);
      } catch (Exception methodException) {
        Debug.LogWarning (methodException.Message + " | " + methodException.StackTrace); 
      }
    }

    public bool IsObjectAllowed (GameObject p_gameObject)
    {
      bool isAllowed = false;

      foreach (string allowedTag in m_allowedEntitiesTag)
      {
        if (p_gameObject.tag.Equals (allowedTag))
        {
          isAllowed = true;
          break;
        }
      }

      return isAllowed;
    }

  }

}
