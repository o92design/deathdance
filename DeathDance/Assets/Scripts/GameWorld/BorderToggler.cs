﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DD_GameWorld
{
  public class BorderToggler : MonoBehaviour
  {
    public List<Border> m_ToggableBorders = new List<Border> ();
    public Color m_openBorder = Color.green;
    public Color m_closedBorder = Color.red;

    public void Update ()
    {
      if(Input.GetButtonDown ("Toggle Border"))
      {
        ToggleAllBorders ();
      }
    }

    public void Start ()
    {
      Initialize ();

      // This is a unit testing workaround for now... TODO (kioskars) proper initialization of BorderToggler
      if(m_ToggableBorders.Count > 0) 
        ToggleAllBorders ();
    }

    public void Initialize ()
    {
      foreach (Border border in FindObjectsOfType<Border> ())
      {
        m_ToggableBorders.Add (border);
      }
    }

    public void ToggleAllBorders ()
    {
      foreach (Border border in m_ToggableBorders)
      {
        ToggleBorder (border);
      }
    }

    private void ToggleBorder (Border p_border)
    {
      if(!p_border.m_allowedEntitiesTag.Contains("Elder"))
      {
        p_border.SetColor (m_openBorder);
        p_border.m_allowedEntitiesTag.Add ("Elder");
      }
      else
      {
        p_border.SetColor (m_closedBorder);
        p_border.m_allowedEntitiesTag.Remove ("Elder");
      }
    }
  }
}


