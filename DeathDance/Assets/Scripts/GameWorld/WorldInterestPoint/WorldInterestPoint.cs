﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DD_GameWorld.DD_WorldInterest
{
  public enum WORLD_INTEREST
  {
    ESCAPE,
    HOME
  }

  public class WorldInterestPoint : MonoBehaviour
  {
    public WORLD_INTEREST m_interest;
    public BoxCollider m_triggerArea;
    public DD_Entity.EntityData m_playerData;

    public void Start ()
    {
      Initialize ();
    }

    public void Initialize ()
    {
      if (!m_triggerArea)
      {
        BoxCollider boxCollider = null;
        foreach (BoxCollider collider in gameObject.GetComponents<BoxCollider> ())
        {
          boxCollider = collider.isTrigger ? collider : null;
        }      

        if (!boxCollider)
        {
          boxCollider = gameObject.AddComponent<BoxCollider> ();
          boxCollider.isTrigger = true;
        }

        m_triggerArea = boxCollider;
      }

      if(!m_playerData)
      {
        foreach (DD_Entity.EntityData entityData in FindObjectsOfType<DD_Entity.EntityData> ())
        {
          if (entityData.m_type == DD_Entity.ENTITY_TYPE.PLAYER)
          {
            m_playerData = entityData;
          }
        }
      }
    }

    public void OnTriggerEnter (Collider other)
    {
      GameObject otherObject = other.gameObject;
      DD_Entity.EntityData entityData = otherObject.GetComponent<DD_Entity.EntityData> ();

      if (entityData)
      {
        Debug.LogFormat ("Entity home = {0} Position = {1}", entityData.m_home.name, entityData.m_home.transform.position);
        Debug.Log ("Point is :" + m_interest);
        switch (m_interest)
        {
          case WORLD_INTEREST.ESCAPE:
          if (entityData.HasInterest (DD_Entity.ENTITY_INTEREST.ESCAPING))
          {
            int reputationLoss = -entityData.m_reputation;
            Debug.LogFormat ("{0} = {1} | has Escaped :(",
                             otherObject.name, otherObject.GetHashCode ());

            Destroy (otherObject);

            m_playerData.ChangeReputation (reputationLoss);
            Debug.LogFormat ("{0} = {1} | lost {2} amount of reputation : reputation at {3}",
                             m_playerData.name, m_playerData.GetHashCode (),
                             -reputationLoss,
                             m_playerData.m_reputation);
          }
          break;
          case WORLD_INTEREST.HOME:
          if(entityData.HasInterest (DD_Entity.ENTITY_INTEREST.HEADING_HOME))
          {
            Debug.LogFormat ("{0} = {1} | has come home :)",
                             otherObject.name, otherObject.GetHashCode ());
            entityData.GetComponent<DD_Entity.BasicMovement> ().m_reachedDestination = true;
            entityData.m_escapeOMeter = 0;
            entityData.m_startEscapeOMeter = true;
          }
          break;
        }
      }
    }
  }
}

