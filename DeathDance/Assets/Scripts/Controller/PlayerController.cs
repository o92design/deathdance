﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DD_Controller
{
  public class PlayerController : MonoBehaviour, IBaseController
  {
    private string m_vertical   = "Vertical"   ;
    private string m_horizontal = "Horizontal" ; 

    public DD_Entity.PlayerMovement m_playerMovement;
    public Camera m_camerToFollow;
    
    public int m_sensetivityY = 15;

    // Modeled after MS Camera Controller all credits to author
    float timeScaleSpeed = 0.0f;
    float rotacX         = 0.0f;
    float rotacY         = 0.0f;
    
    Quaternion temp_xQuaternion;
    Quaternion temp_yQuaternion;
    Quaternion tempRotation;
    Quaternion originalRotation;

    private float m_axis;

    private void Start ()
    {
      Initialize ();
    }

    public void LateUpdate ()
    {
      timeScaleSpeed = Mathf.Clamp (1.0f / Time.timeScale, 0.01f, 1);
      RotateAfterMouse ();
      Move ();
    }

    public void Initialize ()
    {
      m_playerMovement = GetComponent<DD_Entity.PlayerMovement> ();
      
      originalRotation = transform.rotation;      
    }

    public void Move()
    {
      if(Input.GetButton (m_vertical) || Input.GetAxis (m_vertical) != 0)
      {
        m_axis = Input.GetAxis (m_vertical);
        
        if (m_axis > 0)
        {
          Movement (MOVEMENT_DIRECTION.FORWARD);
        }
        else if (m_axis < 0)
        {
          Movement (MOVEMENT_DIRECTION.BACKWARD);
        }
      }

      if(Input.GetButton (m_horizontal) || Input.GetAxis (m_horizontal)  != 0)
      {
        m_axis = Input.GetAxis (m_horizontal);

        if (m_axis > 0)
        {
          Movement (MOVEMENT_DIRECTION.STRAFE_RIGHT);
        }
        else if (m_axis < 0)
        {
          Movement (MOVEMENT_DIRECTION.STRAFE_LEFT);
        }
      }
    }

    public void Movement (MOVEMENT_DIRECTION p_direction)
    {
      m_playerMovement.Move (p_direction);
    }

    // Modeled after MS Camera Controller all credits to author
    public void RotateAfterMouse () 
    {
      rotacX += Input.GetAxis ("Mouse X") * m_sensetivityY;
      rotacY += Input.GetAxis ("Mouse Y") * m_sensetivityY;

      rotacX = ClampAngle (rotacX, -360.0f, 360.0f);
      rotacY = ClampAngle (rotacY, -360.0f, 360.0f);
      temp_xQuaternion = Quaternion.AngleAxis (rotacX, Vector3.up);
      temp_yQuaternion = Quaternion.AngleAxis (rotacY, -Vector3.right);
      tempRotation = originalRotation * temp_xQuaternion;
      transform.localRotation = Quaternion.Lerp (transform.localRotation, tempRotation, Time.deltaTime * 10.0f * timeScaleSpeed);
    }

    private float ClampAngle (float angle, float min, float max)
    {
      if (angle < -360F) { angle += 360F; }
      if (angle > 360F) { angle -= 360F; }
      return Mathf.Clamp (angle, min, max);
    }
  }
}

