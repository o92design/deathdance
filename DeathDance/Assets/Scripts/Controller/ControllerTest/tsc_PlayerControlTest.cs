﻿using UnityEngine;
using UnityEngine.TestTools;
using NUnit.Framework;
using System.Collections;

namespace DD_Controller
{
  public class PlayerControlTest 
  {
    GameObject m_playerObject;
    PlayerController m_playerController;

    private void MovementWithAssertion(MOVEMENT_DIRECTION p_direction)
    {
      Vector3 position = m_playerObject.transform.position;
      m_playerController.Movement (p_direction);
      Debug.LogFormat ("{0} = {1} | Has moved in direction {2}", m_playerController.name, m_playerController.GetHashCode (), p_direction);
      Debug.LogFormat ("{0} | {1}", position, m_playerObject.transform.position);
      Assert.AreNotSame (position, m_playerObject.transform.position, "{0} = {1} | hasn't moved", m_playerController.name, m_playerController.GetHashCode ());
    }

    [SetUp]
    public void SetUp () 
    {
      Debug.Log ("=========================");
      Debug.Log ("TEST - PlayerController");
      Debug.Log ("=========================");

      m_playerObject = new GameObject ("Player");
      m_playerObject.AddComponent<DD_Entity.EntityData> ().InitalizeData  ();
      m_playerObject.AddComponent<DD_Entity.PlayerMovement> ().Initialize ();
      m_playerController = m_playerObject.AddComponent<PlayerController>  ();

      m_playerController.Initialize ();
      Assert.NotNull (m_playerController.m_playerMovement);
    }

    [Test]
    public void MovementDirection ()
    {
      MovementWithAssertion (MOVEMENT_DIRECTION.FORWARD);
      MovementWithAssertion (MOVEMENT_DIRECTION.BACKWARD);
      MovementWithAssertion (MOVEMENT_DIRECTION.STRAFE_LEFT);
      MovementWithAssertion (MOVEMENT_DIRECTION.STRAFE_RIGHT);
    }
  }
}
