﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum MOVEMENT_DIRECTION
{
  FORWARD,
  STRAFE_LEFT,
  STRAFE_RIGHT,
  BACKWARD
}

namespace DD_Entity
{
  public interface IEntityMovement
  {
    EntityData GetEntityData ();
    void Move (MOVEMENT_DIRECTION p_Direction);
    void Move (Vector3 p_direction, int p_speed);
    void BlockedByBorder (bool p_blockedByBorder);
  }
}
