﻿using UnityEngine;
using UnityEngine.TestTools;
using UnityEngine.AI;
using NUnit.Framework;
using System.Collections;

using DD_Entity.DD_Elder;
using DD_Entity;

public class ElderMovementTest 
{
  GameObject m_elderObject;
  int m_ElderObjectHash;
  EntityData m_elderData;
  ElderMovement m_elderMovement;
  NavMeshAgent m_navMeshAgent;

  //////////////////
  /// UTIL FUNCTIONS
  /////////////////

  //////////////////
  /// UTIL FUNCTIONS - END
  /////////////////

  [SetUp]
  public void SetUp ()
  {
    Debug.Log ("=========================");
    Debug.Log ("TEST - ElderMovementTest");
    Debug.Log ("=========================");

    m_elderObject = new GameObject ();
    m_elderObject.name = "Elder TestObject";
    m_ElderObjectHash = m_elderObject.GetHashCode ();
      
    m_navMeshAgent = m_elderObject.AddComponent<NavMeshAgent> ();

    m_elderData = m_elderObject.AddComponent<EntityData> ();
    m_elderData.InitalizeData ();
    m_elderData.m_speed = 200;
    m_elderData.m_home = new GameObject ();

    m_elderObject.AddComponent<Rigidbody> ();
    m_elderMovement = m_elderObject.AddComponent<ElderMovement> ();
    (m_elderMovement as ElderMovement).Initialize ();


    Assert.NotZero (m_elderData.m_speed, "Entity Data hasn't been initialized correctly");
    Assert.NotNull (m_elderMovement, "Elder movement hasn't been initialized");
    Assert.NotNull (m_navMeshAgent, "NavMesh agent hasn't been added");
  }

  [Test]
  public void NewRandomDestination ()
  {
    Vector3 oldRandomDestination = m_elderMovement.m_destination;
    m_elderMovement.NewRandomDestination ();
    Assert.AreNotEqual (oldRandomDestination, m_elderMovement.m_destination, 
                        "{0} = {1} | Hasn't generated a new destination",
                        m_elderObject.name, m_ElderObjectHash);
  }

  [Test]
  public void ReachDestinationUsingWarp ()
  {
    Vector3 oldPosition = m_elderObject.transform.position;
    m_elderMovement.NewRandomDestination ();

    m_navMeshAgent.Warp (m_elderMovement.m_destination);

    Assert.AreEqual (m_elderMovement.m_destination, m_elderObject.transform.position,
                      "{0} = {1} | Has failed to warp to the destination: {2)",
                      m_elderObject.name, m_ElderObjectHash, m_elderMovement.m_destination);
  }
}


