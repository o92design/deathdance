﻿using UnityEngine;
using UnityEngine.TestTools;
using NUnit.Framework;
using System.Collections;
using UnityEngine.AI;

using DD_StateMachine;

namespace DD_Entity.DD_Elder.DD_ElderStateMachine
{
  public class ElderStateMachineTest
  {
    ElderStateMachine m_stateMachine;
    string m_stateMachineName;

    GameObject m_elderObject;
    int m_ElderObjectHash;
    EntityData m_elderData;
    ElderMovement m_elderMovement;
    NavMeshAgent m_navMeshAgent;

    ElderState_Roaming m_roamingState;
    ElderState_Idle m_idleState;

    [SetUp]
    public void SetUp ()
    {
      Debug.Log ("=========================");
      Debug.Log ("TEST - ElderStateMachineTest");
      Debug.Log ("=========================");


      // This part is ugly ... Since it's copied from the ElderMovmenet testcase... TODO: (kioskars)
      // This should be mocked since it isn't really what we're testing.
      m_elderObject = new GameObject ();
      m_elderObject.name = "Elder TestObject";
      m_ElderObjectHash = m_elderObject.GetHashCode ();

      m_navMeshAgent = m_elderObject.AddComponent<NavMeshAgent> ();

      m_elderData = m_elderObject.AddComponent<EntityData> ();
      m_elderData.InitalizeData ();
      m_elderData.m_speed = 200;
      m_elderData.m_home = new GameObject ();

      m_elderObject.AddComponent<Rigidbody> ();
      m_elderMovement = m_elderObject.AddComponent<ElderMovement> ();
      (m_elderMovement as ElderMovement).Initialize ();
      //////// END of ugliness

      m_stateMachine = m_elderObject.AddComponent<ElderStateMachine> ();
      m_stateMachine.m_name = "Elder StateMachine";
      m_stateMachineName = m_stateMachine.m_name;

      m_roamingState = new ElderState_Roaming ();
      m_idleState = new ElderState_Idle ();

      m_roamingState.Initialize ();
      m_idleState.Initialize ();
    }

    [Test]
    public void SetCurrentState ()
    {
      Assert.True (m_stateMachine.SetCurrentState (m_roamingState),
                   "{0} was unable to set the current state to {1}",
                   m_stateMachineName, m_roamingState.GetName ());

      Assert.NotNull (m_stateMachine.GetCurrentState (),
                      "{0} current state was null",
                      m_stateMachineName);

      Assert.False (m_stateMachine.SetCurrentState (m_roamingState),
                    "{0} set current state to the same state it was | {1}",
                    m_stateMachineName, m_roamingState.GetName ());
    }

    [Test]
    public void AddAllowedEntryState ()
    {
      Assert.True (m_idleState.AddToAllowedEntryStates (new ElderState (ElderState.STATE.ESCAPING)),
                   "{0} should have been inserted as allowed entry state since it isn't in the container of {1}",
                   ElderState.STATE.ESCAPING, m_idleState.GetName ());

      Assert.False (m_idleState.AddToAllowedEntryStates (m_roamingState),
                   "{0} should not have been inserted as allowed entry state since it is already in the container of {1}",
                   m_roamingState.GetName (), m_idleState.GetName ());
    }

    [Test]
    public void SwitchState ()
    {
      m_stateMachine.SwitchState (m_roamingState);
      Assert.NotNull (m_stateMachine.GetCurrentState ());

      Assert.False (m_stateMachine.SwitchState (m_roamingState),
                    "{0} switched to the same state | {1}",
                    m_stateMachineName, m_roamingState.GetName ());

      Assert.True (m_stateMachine.SwitchState (m_idleState),
                   "{0} was unable to switch the state from {1} to {2}",
                   m_stateMachineName, m_roamingState.GetName (), m_idleState.GetName ());
    }

    [Test]
    public void UpdateState ()
    {
      m_stateMachine.SwitchState (new ElderState (ElderState.STATE.TALKING));

      // Now it would have been a great oppurtunity to use a mock for movement... TODO: (kioskars)
      m_stateMachine.UpdateState ();
      m_stateMachine.SwitchState (new ElderState (ElderState.STATE.ESCAPING));
      m_stateMachine.UpdateState ();

    }
  }
}

