﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

using DD_StateMachine;

namespace DD_Entity
{
  namespace DD_Elder
  {
    namespace DD_ElderStateMachine
    {
      public class ElderStateMachine : IStateMachine
      {
        public List<ElderState> m_states;
        public EntityData m_entityData;

        private ElderState.STATE m_state;

        public Text m_stateTextValue;

        private int m_stateIndex = 1;

        public ElderStateMachine (string p_name) 
        { 
          m_name = p_name; 
        }

        public void Start ()
        {
          Initialize ();
          StartCoroutine (ChangeStateRandomly ());
        }

        public void Update ()
        {
          if (Input.GetKeyDown (KeyCode.N)) SwitchState (m_states[m_stateIndex++ % m_states.Count]);
          if (Input.GetKeyDown (KeyCode.M)) 
          { 
            SwitchState (m_states[3]); // Go to home state
            m_entityData.m_escapeOMeter = 0;
          } 

          if (m_entityData.m_escapeOMeter >= 100 && m_state != ElderState.STATE.ESCAPING)
          {
            SwitchState (m_states[2]);
          }

          if(m_currentState.GetState () == (int)ElderState.STATE.GOING_HOME)
          {
            if(((ElderState)m_currentState).m_movement.m_reachedDestination)
            {
              Debug.Log ("Switched state to ROAMING state since elder came home");
              SwitchState (m_states[1]);
            }
          }

          m_stateTextValue.text = m_state.ToString ();
        }

        public void LateUpdate ()
        {
          UpdateState ();
          if(m_entityData.m_startEscapeOMeter)
          {
            StartCoroutine (ChangeEscapeOMeter ());
            m_entityData.m_startEscapeOMeter = false;
          }
        }

        public IEnumerator ChangeEscapeOMeter ()
        {
          for(; m_entityData.m_escapeOMeter < 100; )
          {
            yield return new WaitForSeconds (Random.Range (1, 1));
            m_entityData.m_escapeOMeter += 5;
          }
        }

        public IEnumerator ChangeStateRandomly ()
        {
          for (; m_state != ElderState.STATE.ESCAPING; )
          {

            Debug.Log ("STATE IS: " + m_state);
            int minTime = 1;
            int maxTime = 3;

            yield return new WaitForSeconds (Random.Range (minTime, maxTime));
            int state = Random.Range (0, 3);

            // Switch to state unless it's escaping state ( 2 ) Uglyyyy :/
            if (state != 2) SwitchState (m_states[state]);

            if (m_state == ElderState.STATE.ESCAPING) break;
          }
        }

        public void Initialize ()
        {
          m_states = new List<ElderState> ();
          m_entityData = GetComponent<EntityData> ();

          ElderState_Idle idleState = new ElderState_Idle ();
          ElderState_Roaming roamingState = new ElderState_Roaming ();
          ElderState escapingState = new ElderState (ElderState.STATE.ESCAPING);
          ElderState_GoingHome goingHome = new ElderState_GoingHome ();

          idleState.AddToAllowedEntryStates (escapingState);
          roamingState.AddToAllowedEntryStates (escapingState);
          escapingState.AddToAllowedEntryStates (goingHome);

          m_states.Add (idleState);
          m_states.Add (roamingState);
          m_states.Add (escapingState);
          m_states.Add (goingHome);

          for(int stateIndex = 0; stateIndex < m_states.Count; ++stateIndex)
          {
            m_states[stateIndex].Initialize ();
            m_states[stateIndex].m_movement = GetComponent<ElderMovement> ();
          }

          SwitchState (roamingState);
        }

        public override bool SwitchState (IState p_toState)
        {
          bool switchedState = false;
          if (m_currentState == null) 
          { 
            SetCurrentState (p_toState); 
            switchedState = true; 
          }
          else
          {
            switchedState = SetCurrentState (p_toState);
          }

          m_state = (ElderState.STATE)m_currentState.GetState ();

          return switchedState;
        }
      }
    }
  }
}
