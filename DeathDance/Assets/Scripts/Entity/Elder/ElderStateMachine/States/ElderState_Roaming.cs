﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DD_Entity.DD_Elder.DD_ElderStateMachine
{
  public class ElderState_Roaming : ElderState
  {
    public ElderState_Roaming () { m_state = STATE.ROAMING; m_name = m_state + " state"; }

    public override bool Initialize ()
    {
      if(!m_isInitialized)
      {
        AddToAllowedEntryStates (new ElderState_Idle (), new ElderState_GoingHome ());
        m_isInitialized = true;
      }

      return m_isInitialized;
    }

    new public bool Enter ()
    {
      base.Enter ();
      if(!m_isInitialized)
      {
        Initialize ();
      }

      return true;
    }

    new public bool Exit ()
    {
      base.Enter ();

      return true;
    }

    public override void Update ()
    {
      m_movement.StartRoaming ();
    }
  }
}
