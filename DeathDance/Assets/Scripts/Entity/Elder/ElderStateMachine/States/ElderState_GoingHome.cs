﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DD_Entity.DD_Elder.DD_ElderStateMachine
{
  public class ElderState_GoingHome : ElderState
  {
    public ElderState_GoingHome () { m_state = STATE.GOING_HOME; m_name = m_state + " state"; }

    public override bool Initialize ()
    {
      if (!m_isInitialized)
      {
        AddToAllowedEntryStates (new ElderState_Idle (), new ElderState_Roaming (), new ElderState (STATE.ESCAPING));
        m_isInitialized = true;
      }

      return m_isInitialized;
    }

    public override void Enter ()
    {
      base.Enter ();
      if (!m_isInitialized)
      {
        Initialize ();
      }
      m_movement.m_navMeshAgent.speed = 3;
      m_movement.GetEntityData ().m_interests.Add (ENTITY_INTEREST.HEADING_HOME);
      Debug.Log (m_movement.GetEntityData ().m_interests.Count);
    }

    public override void Exit ()
    {
      base.Enter ();

      m_movement.GetEntityData ().m_interests.Remove (ENTITY_INTEREST.HEADING_HOME);
      Debug.Log (m_movement.GetEntityData ().m_interests.Count);

      m_movement.m_reachedDestination = false;
    }

    public override void Update ()
    {
      if(!m_movement.m_reachedDestination)
      {
        m_movement.GoHome ();
      }
    }
  }
}

