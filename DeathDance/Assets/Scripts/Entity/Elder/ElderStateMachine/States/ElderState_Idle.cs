﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DD_Entity.DD_Elder.DD_ElderStateMachine
{
  public class ElderState_Idle : ElderState
  {
    public ElderState_Idle () { m_state = STATE.IDLE; m_name = m_state + " state"; }

    public override bool Initialize ()
    {
      AddToAllowedEntryStates (new ElderState_Roaming (), new ElderState_GoingHome ());
      return true;
    }

    new public bool Enter ()
    {
      base.Enter ();

      return true;
    }

    public override void Update ()
    {
      m_movement.StopAllCoroutines ();

      m_movement.Stop ();
    }
  }

}
