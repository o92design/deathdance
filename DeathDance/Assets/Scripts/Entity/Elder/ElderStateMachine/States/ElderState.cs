﻿using System.Collections;
using System.Collections.Generic;
using DD_StateMachine;
using UnityEngine;

using DD_GameWorld.DD_WorldInterest;

namespace DD_Entity
{
  namespace DD_Elder.DD_ElderStateMachine
  {
    public class ElderState : IState
    {
      public enum STATE
      {
        // This is ugly... Since a state has to be defined of what it do. TODO: (kioskars)
        // However I have it here for now and will set that as default... for now ;)
        NONE         , 
        GOING_HOME ,
        ESCAPING     ,
        TALKING      ,
        ROAMING      ,
        IDLE         ,
        RESTING      ,
      }

      public string m_name { get; set; }

      public string GetName () { return m_name; }
      public void SetName (string p_name) { m_name = p_name; }

      protected STATE m_state;
      protected bool m_isInitialized { get; set; }

      private List<STATE> m_allowedEntryStates = new List<STATE> ();
      public ElderMovement m_movement { get; set; }

      public ElderState () { m_state = STATE.NONE; m_name = m_state.ToString () + " state"; }
      public ElderState (STATE p_state) { m_state = p_state; m_name = m_state.ToString () + " state"; }

      public virtual bool Initialize () { m_isInitialized = true;  return m_isInitialized; }

      public virtual void Enter ()
      {
        Debug.Log ("Entering " + m_name);

        if(!m_isInitialized)
        {
          m_isInitialized = Initialize ();
        }

        if(m_state == STATE.ESCAPING)
        {
          m_movement.GetEntityData ().m_interests.Add (ENTITY_INTEREST.ESCAPING);
        }
      }

      public virtual void Exit ()
      {
        Debug.Log ("Exited " + m_name);

        if (m_state == STATE.ESCAPING)
        {
          m_movement.GetEntityData ().m_interests.Remove (ENTITY_INTEREST.ESCAPING);
        }

        m_isEscaping = false;
      }

      public int GetState () { return (int) m_state;  }
      public bool IsAllowedToEnterState (IState p_toState)
      {
        STATE state = (STATE) p_toState.GetState ();

        Debug.Log ("Checking if we're allowed to go from " + m_name + " to " + p_toState.GetName ());

        if (state == m_state)
        {
          Debug.Log ("Could not enter " + m_name + " since it's already in that state!");
          return false;
        }

        foreach (STATE checkState in m_allowedEntryStates)
        {
          if(checkState == state)
          {
            return true;
          }
        }

        return false;
      }

      // This is bad allocation of memory and we're violating it since we only use the m_state variable. TODO (kioskars)
      // Possible option: refactor whole state machine to only specify elder movement state ??
      public bool AddToAllowedEntryStates (IState p_state)
      {
        STATE state = (STATE)p_state.GetState ();

        foreach (STATE checkState in m_allowedEntryStates)
        {
          if (checkState == state)
          {
            return false;
          }
        }

        m_allowedEntryStates.Add (state);
        return true;
      }

      // Will return false if one or more additions failed.
      public bool AddToAllowedEntryStates (params IState[] p_states)
      {
        bool successFullAddition = true;
        foreach (IState state in p_states)
        {
          if (!AddToAllowedEntryStates (state)) successFullAddition = false;
        }

        return successFullAddition;
      }

      // This is only a temporary style... TODO: (kioskars) Make a escaping state class
      bool m_isEscaping = false;

      /// <summary>
      /// This is for behaviour that doesn't rely heavily on being "selfaware"
      /// I.E just template fill out for now.
      /// <para/>
      /// For example we could have easily just have this class act as all states and depending on its m_state 
      /// It'll execute the behaviour inside the related switch case.
      /// </summary>
      public virtual void Update () 
      {
        switch (m_state)
        {
          case STATE.TALKING:
          case STATE.RESTING:
          case STATE.NONE:
          break;
          case STATE.ESCAPING:
          if(!m_isEscaping)
          {
            m_isEscaping = true;
            int increaseSpeed = 10;
            m_movement.m_navMeshAgent.speed += increaseSpeed;
            m_movement.StartEscaping ();
          }

          if (m_movement.m_destination != m_movement.m_escapePoint.transform.position)
          {
            m_movement.m_destination = m_movement.m_escapePoint.transform.position;
            m_movement.m_navMeshAgent.SetDestination (m_movement.m_destination);
          }

          Debug.LogFormat ("{0} = {1} | is heading to {2} since it is ESCPAING !!!! waaaah",
                             m_movement.gameObject.name, m_movement.gameObject.GetHashCode (),
                             m_movement.m_destination);

          break;

          // Roaming and Idle states will not execute since we have them classified and override this update method :)
          // This is all for show wee hoo !
          case STATE.GOING_HOME:
          case STATE.ROAMING:
          m_movement.StartRoaming ();
          break;
          case STATE.IDLE:
          m_movement.StopAllCoroutines ();
          m_movement.Stop ();
          break;
        } 
      }
    }
  }
}
