﻿using System.Collections;
using System.Collections.Generic;
using System.Threading;
using UnityEngine;
using UnityEngine.AI;

using DD_GameWorld.DD_WorldInterest;

namespace DD_Entity
{
  namespace DD_Elder
  {
    public class ElderMovement : BasicMovement
    {
      public NavMeshAgent m_navMeshAgent;

      public bool m_viablePath = true;

      public float m_dizzinessInSeconds = 3;
      public float m_minimumRoamingAreaX = -50;
      public float m_maximumRoamingAreaX = 50;
      public float m_minimumRoamingAreaZ = -50;
      public float m_maximumRoamingAreaZ = 50;

      public Vector3 m_destination;
      public Transform m_homePoint;
      public Transform m_escapePoint;

      public void Start ()
      {
        Initialize ();
        NewRandomDestination ();
      }

      public override void Initialize ()
      {
        base.Initialize ();

        try
        {
          m_navMeshAgent = GetComponent<NavMeshAgent> ();
        }
        catch (MissingComponentException missingException)
        {
          Debug.LogWarningFormat ("{0} = {1} | is missing an important component: {2}", name, GetHashCode (), missingException.Message);
        }

        if(!m_homePoint)
        {
          m_homePoint = GetEntityData ().m_home.transform;
        }

        m_escapePoint = GameObject.FindObjectsOfType<WorldInterestPoint> ()[1].transform;
      }

      public void GoToDestination ()
      {
        m_viablePath = m_navMeshAgent.CalculatePath (m_destination, m_navMeshAgent.path);

        if (m_viablePath)
        {
          m_navMeshAgent.SetDestination (m_destination);
        }
        else
        {
          m_navMeshAgent.isStopped = true;
        }
      }

      public void StartRoaming ()
      {
        StartCoroutine (Roaming (true));
      }

      public void GoHome ()
      {
        if (m_navMeshAgent.isStopped)
        {
          Debug.Log ("Elder has stopped moving");
          m_navMeshAgent.ResetPath ();
          GoToDestination ();
        }
        else
        {
          if (m_destination != m_homePoint.position) m_destination = m_homePoint.position;
          GoToDestination ();
        }
      }

      public void StartEscaping ()
      {
        StartCoroutine (Roaming (false));
      }

      // Will start to move into the destination but then will "dizzy" stop and calculate path.
      // This is because we wan't to simulate dimentia and "forgetting" where to go for a little while.
      private IEnumerator Roaming (bool setRandomDestination)
      {
        Debug.Log ("NAVMESH DESTINATION: " + m_navMeshAgent.destination);

        if (m_navMeshAgent.isStopped)
        {
          Debug.Log ("Elder has stopped moving");
          m_navMeshAgent.ResetPath ();
          GoToDestination ();
          yield return null;
        }

        if (!m_viablePath || transform.position == m_navMeshAgent.destination)
        {
          float dizziness = Random.Range (0, m_dizzinessInSeconds);
          Debug.Log ("Will be dizzy for " + dizziness + " seconds");
          yield return new WaitForSeconds (dizziness);

          if(setRandomDestination){
            NewRandomDestination ();
          }

          GoToDestination ();
        }
      }

      public void NewRandomDestination ()
      {
        // TODO: (KiOskars) Fix the hardcoding test variables for the area of roaming.
        m_destination = new Vector3 (Random.Range (m_minimumRoamingAreaX, m_maximumRoamingAreaX), 0, Random.Range (m_minimumRoamingAreaZ, m_maximumRoamingAreaZ));
      }

      public void Stop ()
      {
        m_navMeshAgent.isStopped = true;
      }
    }
  }
}
