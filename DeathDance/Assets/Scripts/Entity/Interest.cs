﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DD_Entity
{
  public enum ENTITY_INTEREST
  {
    ESCAPING,
    NURSING,
    TALKING,
    HEADING_HOME
  }
}
