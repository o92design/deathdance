﻿using UnityEngine;
using UnityEngine.TestTools;
using NUnit.Framework;
using System.Collections;

namespace DD_Entity
{
  public class EntityMovementTest
  {
    EntityData m_entityData;
    IEntityMovement m_entityMovement;
    GameObject m_entityObject;
    int m_entityObjectHash;

    //////////////////
    /// UTIL FUNCTIONS
    /////////////////    

    private void Move (MOVEMENT_DIRECTION p_direction)
    {
      Debug.Log ("Move Direction: " + p_direction);
      Vector3 oldPosition = m_entityObject.transform.position;

      m_entityMovement.Move (p_direction);
      Debug.Log ("New Position: " + m_entityObject.transform.position);

      Assert.AreNotEqual (oldPosition, m_entityObject.transform.position, 
                          "{0} = {1} | Hasn't moved from position {2}",
                          m_entityObject.name,
                          m_entityObjectHash,
                          oldPosition);
    }

    private void MoveWithGivenSpeed (MOVEMENT_DIRECTION p_direction, int p_speed)
    {
      Vector3 position = m_entityObject.transform.position;
      Debug.LogFormat ("Moving in {0} direction with given speed of {1} | Current position: {2}", 
                       p_direction, 
                       p_speed,
                       position);

      switch (p_direction)
      {
        case MOVEMENT_DIRECTION.FORWARD:
        m_entityMovement.Move (Vector3.forward, p_speed);
        break;
        case MOVEMENT_DIRECTION.BACKWARD:
        m_entityMovement.Move (Vector3.back, p_speed);
        break;
        case MOVEMENT_DIRECTION.STRAFE_LEFT:
        m_entityMovement.Move (Vector3.left, p_speed);
        break;
        case MOVEMENT_DIRECTION.STRAFE_RIGHT:
        m_entityMovement.Move (Vector3.right, p_speed);
        break;
      }

      Debug.Log (m_entityObject.transform.position);
      Assert.AreNotSame (position, m_entityObject.transform.position, 
                         "{0} = {1} | Hasn't moved from position {2}", 
                         m_entityObject.name,
                         m_entityObjectHash, 
                         position);
    }

    //////////////////
    /// UTIL FUNCTIONS - END
    /////////////////

    [SetUp]
    public void SetUp ()
    {
      Debug.Log ("=========================");
      Debug.Log ("TEST - EntityMovementTest");
      Debug.Log ("=========================");

      m_entityObject = new GameObject ();
      m_entityObject.name = "Entity TestObject";
      m_entityObjectHash = m_entityObject.GetHashCode ();

      m_entityData = m_entityObject.AddComponent<EntityData> ();
      m_entityData.InitalizeData ();
      m_entityData.m_speed = 200;

      m_entityObject.AddComponent<Rigidbody> ();
      m_entityMovement = m_entityObject.AddComponent<PlayerMovement> ();
      (m_entityMovement as PlayerMovement).Initialize ();

      Assert.NotZero (m_entityData.m_speed, "Entity Data hasn't been initialized correctly");
      Assert.NotNull (m_entityMovement, "Player movement hasn't been initialized");
    }

    [Test]
    public void MoveInAllDirections ()
    {
      Move (MOVEMENT_DIRECTION.FORWARD);
      Assert.Greater (m_entityObject.transform.position.z, 0);
      
      Move (MOVEMENT_DIRECTION.BACKWARD);
      Move (MOVEMENT_DIRECTION.BACKWARD);
      Assert.Less (m_entityObject.transform.position.z, 0);

      Move (MOVEMENT_DIRECTION.STRAFE_LEFT);
      Assert.Less (m_entityObject.transform.position.x, 0);

      Move (MOVEMENT_DIRECTION.STRAFE_RIGHT);
      Move (MOVEMENT_DIRECTION.STRAFE_RIGHT);
      Assert.Greater (m_entityObject.transform.position.x, 0);
    }

    [Test]
    public void MoveForward ()
    {
      Move (MOVEMENT_DIRECTION.FORWARD);
    }

    [Test]
    public void MoveBackward ()
    {
      Move (MOVEMENT_DIRECTION.BACKWARD);
    }

    [Test]
    public void MoveStrafeLeft ()
    {
      Move (MOVEMENT_DIRECTION.STRAFE_LEFT);
    }

    [Test]
    public void MoveStrafeRight ()
    {
      Move (MOVEMENT_DIRECTION.STRAFE_RIGHT);
    }

    [Test]
    public void MoveWithGivenSpeed ()
    {
      MoveWithGivenSpeed (MOVEMENT_DIRECTION.FORWARD, 50);
      MoveWithGivenSpeed (MOVEMENT_DIRECTION.BACKWARD, 10);
      MoveWithGivenSpeed (MOVEMENT_DIRECTION.STRAFE_LEFT, 100);
      MoveWithGivenSpeed (MOVEMENT_DIRECTION.STRAFE_RIGHT, 25);
    }
  }
}

