﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace DD_Entity 
{
  public enum ENTITY_TYPE
  {
    PLAYER,
    ELDER,
    NURSE
  }
  public class EntityData : MonoBehaviour
  {
    private bool isInitialized = false;
    public ENTITY_TYPE m_type;

    public List<ENTITY_INTEREST> m_interests;
    public GameObject m_home;

    [Range(10, 50)]
    public int m_speed = 10;

    public int m_escapeOMeter = 0;
    public bool m_startEscapeOMeter = true;

    public int m_reputation = 0;

    public Text m_escapeTextValue;
    
    public void Start ()
    {
      InitalizeData ();
    }

    public bool InitalizeData(int p_speed = 10) 
    {
      bool alreadyInitialized = isInitialized;

      if(!isInitialized)
      {
        isInitialized = true;
        m_speed = p_speed;

        m_interests = new List<ENTITY_INTEREST> ();
      }

      return !alreadyInitialized;
    }

    public bool HasInterest (ENTITY_INTEREST p_interest)
    {
      return m_interests.Contains (p_interest);
    }

    public void ChangeReputation (int p_amount)
    {
      m_reputation += p_amount;
    }

    public void Update ()
    {
      // Need to have this check since Player max won't have any text asset.. :(
      if(m_escapeTextValue) m_escapeTextValue.text = m_escapeOMeter.ToString ();
    }
  }
}

