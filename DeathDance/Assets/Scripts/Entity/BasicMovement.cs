﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DD_Entity
{
  public abstract class BasicMovement : MonoBehaviour, IEntityMovement
  {
    protected EntityData m_entityData;

    public bool m_reachedDestination = false;
    public virtual void Initialize () { GetEntityData (); }

    public EntityData GetEntityData ()
    {
      if (m_entityData)
      {
        return m_entityData;
      }

      try
      {
        m_entityData = GetComponent<EntityData> ();
      }
      catch (MissingComponentException missingComponentException)
      {
        Debug.LogErrorFormat ("{0} = {1} | missing  entity data script", name, GetInstanceID ());
        Debug.LogError (missingComponentException.StackTrace);
      }

      return m_entityData;
    }

    public void Move (Vector3 p_direction, int p_speed)
    {
      transform.Translate(p_direction * p_speed * Time.deltaTime);
    }

    public void Move (MOVEMENT_DIRECTION p_Direction)
    {
      switch (p_Direction)
      {
        case MOVEMENT_DIRECTION.FORWARD:
        Move (Vector3.forward, m_entityData.m_speed);
        break;

        case MOVEMENT_DIRECTION.BACKWARD:
        Move (Vector3.back, m_entityData.m_speed);
        break;

        case MOVEMENT_DIRECTION.STRAFE_LEFT:
        Move (Vector3.left, m_entityData.m_speed);
        break;

        case MOVEMENT_DIRECTION.STRAFE_RIGHT:
        Move (Vector3.right, m_entityData.m_speed);
        break;
      }
    }

    public virtual void BlockedByBorder (bool p_blockedByBorder)
    {
    }
  }
}

