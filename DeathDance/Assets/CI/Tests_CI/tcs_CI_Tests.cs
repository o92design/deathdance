﻿using UnityEngine;
using UnityEngine.TestTools;
using NUnit.Framework;
using System.Collections;

public class TCS_CI
{

  [Test]
  public void BasciCITestCase ()
  {
    // Use the Assert class to test conditions.
    int life = 65;
    int expectedLife = 59;

    life--;
    life -= 5;

    Assert.AreEqual (expectedLife, life);
  }

  // A UnityTest behaves like a coroutine in PlayMode
  // and allows you to yield null to skip a frame in EditMode
  [UnityTest]
  public IEnumerator NewTestScriptWithEnumeratorPasses ()
  {
    // Use the Assert class to test conditions.
    // yield to skip a frame
    yield return null;
  }
}
