﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using System.IO;

public class CI_Build : MonoBehaviour
{

  static string[] SCENES = FindEnabledEditorScenes ();

  static string APP_NAME = "DeathDance";
  static string TARGET_DIR = "D:/unity/projects/" + APP_NAME;

  [MenuItem("CI/Build Windows Release")]
  public static void PerformWindowsBuild ()
  {
    string targetDir = TARGET_DIR + "/builds_windows/release/";
    GenericBuild (SCENES,
                  targetDir,
                  ".exe",
                  BuildTarget.StandaloneWindows64,
                  BuildTargetGroup.Standalone,
                  BuildOptions.StrictMode);
  }

  [MenuItem("CI/Build Windows Dev")]
  public static void PerformWindowsDevelopmentBuild ()
  {
    string targetDir = TARGET_DIR + "/builds_windows/dev/";

    GenericBuild (SCENES,
                  targetDir,
                  "_dev.exe",
                  BuildTarget.StandaloneWindows64,
                  BuildTargetGroup.Standalone,
                  BuildOptions.Development | BuildOptions.AllowDebugging);
  }

  private static string[] FindEnabledEditorScenes ()
  {
    List<string> EditorScenes = new List<string> ();
    foreach (EditorBuildSettingsScene scene in EditorBuildSettings.scenes)
    {
      if (!scene.enabled) continue;
      EditorScenes.Add (scene.path);
    }
    return EditorScenes.ToArray ();
  }

  static void GenericBuild (string[] scenes,
                            string target_dir,
                            string targetEnding,
                            BuildTarget target, 
                            BuildTargetGroup targetGroup, 
                            BuildOptions buildOptions,
                            bool includeBuildNumber = false)
  {
    if(includeBuildNumber)
    {
      var args = System.Environment.GetCommandLineArgs ();
      string buildNumber = args[args.Length - 1];
      Directory.CreateDirectory (TARGET_DIR + "_" + buildNumber);
      target_dir += APP_NAME + "_" + buildNumber + "/" + APP_NAME;
      target_dir += "_" + buildNumber + targetEnding;
    }
    else
    {
      Directory.CreateDirectory (TARGET_DIR);
      target_dir += APP_NAME + "/" + APP_NAME;
      target_dir += targetEnding;
    }
    
    BuildPlayerOptions playerOptions = new BuildPlayerOptions ();
    playerOptions.target = target;
    playerOptions.scenes = scenes;
    playerOptions.locationPathName = target_dir;
    playerOptions.options = buildOptions;

    EditorUserBuildSettings.SwitchActiveBuildTarget (targetGroup, target);
    BuildPipeline.BuildPlayer (playerOptions);
  }
}
